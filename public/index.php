<?php

use App\Advisor;
use App\Events\ReviewInvalid;
use App\Review;

require '../vendor/autoload.php';

try
{
    $input = json_decode(file_get_contents('../data/input.json'), true);

    if ($input === false || $input === null)
    {
        throw new Exception("Input file missing or empty.");
    }

    $filteredInput = array_filter($input, function ($data)
    {
        if (!$data["datetime"])
        {
            event(new ReviewInvalid("Could not read review summary data: `datetime` missing or invalid"));
            return false;
        }

        if (!$data["advisor"])
        {
            event(new ReviewInvalid("Could not read review summary data: `advisor` missing or invalid"));
            return false;
        }

        if (!$data["tag"])
        {
            event(new ReviewInvalid("Could not read review summary data: `tag` missing or invalid"));
            return false;
        }

        if (!$data["wordcount"])
        {
            event(new ReviewInvalid("Could not read review summary data: `wordcount` missing or invalid"));
            return false;
        }

        if (!preg_match(Review::WORDCOUNT_PATTERN, $data["wordcount"]))
        {
            event(new ReviewInvalid("Could not read review summary data: `wordcount` missing or invalid"));
            return false;
        }

        if (!in_array($data["solicited"], ["solicited", "unsolicited"]))
        {
            event(new ReviewInvalid("Could not read review summary data: `solicited` missing or invalid"));
            return false;
        }

        if (!$data["rating"])
        {
            event(new ReviewInvalid("Could not read review summary data: `rating` missing or invalid"));
            return false;
        }

        if (!preg_match(sprintf("/^[%d-%d]$/", Review::RATING_MIN, Review::RATING_MAX), strlen($data["rating"])))
        {
            event(new ReviewInvalid("Could not read review summary data: `rating` missing or invalid"));
            return false;
        }

        return true;
    });

    $jon = new Advisor("Jon");

    $jon->setReviews(array_map(function ($input) {
            return new Review($input);
        }, $filteredInput)
    );
}
catch (Exception $e)
{
    echo "<h3>Error exception</h3><hr>";
    echo "\n\n<pre>" . print_r($e->getMessage(), TRUE) . "</pre>\n\n";
}
