# Technical skills assessment

## Requirements
1. php >= 7.0.0
2. composer (no 3rd party libraries, just for autoloading)

## Installation

1. git clone project
2. run composer install
3. projects entry point is `public/index.php` file. Configure your vhost to serve files from project's `public`
directory or navigate your browser to project/public eg. `http://localhost/public/` 

## Application overview

Application is written in PHP and follows best practice pinciples in object oriented design.

Following design patterns are implemented:

1. strategy pattern - resolves complexity of manipulating score by delegating the application of score indicators to Score class
2. event system - simplified observer pattern for handling output

## Application output:

```
Could not read review summary data: `tag` missing or invalid
Info: Jon has a trusted review score of: 100.0. Indicators applied: [App\Indicators\Solicited => 3.0 | App\Indicators\AllStar => -2.0] 
Info: Jon has a trusted review score of: 80.0. Indicators applied: [App\Indicators\BurstSameHour => -20.0] 
Info: Jon has a trusted review score of: 79.5. Indicators applied: [App\Indicators\LotsToSay => -0.5] 
Info: Jon has a trusted review score of: 74.5. Indicators applied: [App\Indicators\Solicited => 3.0 | App\Indicators\AllStar => -2.0 | App\Indicators\AllStar => -2.0 | App\Indicators\AllStar => -2.0 | App\Indicators\AllStar => -2.0] 
Info: Jon has a trusted review score of: 77.5. Indicators applied: [App\Indicators\Solicited => 3.0] 
Warning: Jon has a trusted review score of 50.5
Alert: Jon has been de-activated due to a low trusted review score
```

Output lists applied score indicators for debugging purposes, easier understanding what is going on and how it works.

# Challenge description

This exercise is to build a simple tool that can build up integrity score for a professional 
advisor based on attributes of the reviews they receive.  
 
Everyone starts of with an integrity score of 100% which is affected by the indicators below. 
 
When the score goes below 70% then the tool should warn a customer service agent to go and investigate.
If it goes below 50% then the professional is deactivated and an alert 
raised. 

## Negative Indicators: 
 
**Lots to say:**  Genuine reviewers tend to say less - knock 0.5% points off for each review that 
contains more than 100 words. 
 
**Burst:**  If a number of reviews come in within the same time frame - knock 40% points off if 
2 or more come through in the same minute, 20% points if they come through in the same 
hour. 
 
**Same Device:**  We have a system that forms a readable tag (e.g. LB4-6WR) based on the 
browser/device/location. If we are seeing multiple reviews coming from the same device 
knock 30% points off each time. 
 
**All-Star:**  Non-genuine reviews are likely to have a five star rating take 2% points off the 
integrity score for each review that has 5 stars; quadruple the penalty if the average is 
under 3.5 stars.
 
## Positive Indicators: 
 
**Solicited:**  If the review was left by someone who was invited by the professional then add 
3% points to the integrity score. 
1 of 3  
 
The tool should produce the following outputs for the inputs. 

## Test input
```
12th July 12:04, Jon, solicited, LB3‐TYU, 50 words, *****
12th July 12:05, Jon, unsolicited, KB3‐IKU, 20 words, **
13th July 15:04, Jon, unsolicited, CY8‐IPK, 150 words, ***
15th July 10:04, Jon, solicited, BB4‐IPK, 40 words, *****
15th July 15:09, Jon, monkey
29th August 10:04, Jon, solicited, LX2‐IPK, 70 words, ****
2nd September 10:04, Jon, solicited, KB3‐IKU, 50 words, ****
2nd September 10:04, Jon, solicited, AN9‐IPK, 90 words, **
```

## Test output
```
Info: Jon has a trusted review score of 100
Info: Jon has a trusted review score of 80 
Info: Jon has a trusted review score of 79.5 
Info: Jon has a trusted review score of 74.5 
Could not read review summary data 
Info: Jon has a trusted review score of 77.5 
Warning: Jon has a trusted review score of 50.5 
Alert: Jon has been de‐activated due to a low trusted review score 
```

## How to pass this test:

1. Submit production quality code - something that runs, is easy to maintain and add new features. 
2. Don't gold plate 
3. No frameworks, external libraries or tools except for testing and building your code
(e.g., Ant, Rspec, Rake, Jasmine, Karma, Jake, Gulp etc.) 
4. Briefly explain your design decisions - let us know your thinking

