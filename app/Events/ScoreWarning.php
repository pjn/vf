<?php

namespace App\Events;

class ScoreWarning extends Event
{
    protected $status = "Warning";
}
