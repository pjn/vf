<?php

namespace App\Events;

use DateTime;

class Event
{
    protected $datetime;
    protected $message;
    protected $status = "Info";

    public function __construct($message="")
    {
        $this->datetime = new DateTime();
        $this->message = $message;
    }

    public function fire()
    {
        if ($this->status)
        {
            printf("<pre>%s: %s</pre>", $this->status, $this->message);
            return;
        }

        printf("<pre>%s</pre>", $this->message);
    }
}
