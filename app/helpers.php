<?php

use App\Events\Event;

$events = [];

function event(Event $event)
{
    $events[] = $event;
    $event->fire();
}
