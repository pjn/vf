<?php

namespace App\Indicators;


/**
 * Class SameDevice
 * We have a system that forms a readable tag (e.g. LB4-6WR) based on the browser/device/location.
 * If we are seeing multiple reviews coming from the same device knock 30% points off each time.
 */
class SameDevice extends AbstractIndicator
{
    protected $value = -30;
}
