<?php

namespace App\Indicators;

class AbstractIndicator implements Indicator
{
    protected $value = 0;

    public function __toString()
    {
        return sprintf("%.1f", $this->value);
    }

    public function apply()
    {
        return $this->value;
    }
}
