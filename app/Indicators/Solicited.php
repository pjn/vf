<?php

namespace App\Indicators;


/**
 * Class Solicited
 * If the review was left by someone who was invited by the professional then add 3% points to the integrity score.
 */
class Solicited extends AbstractIndicator
{
    protected $value = 3;
}
