<?php

namespace App\Indicators;


interface Indicator
{
    public function apply();
}
