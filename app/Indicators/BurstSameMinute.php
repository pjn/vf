<?php

namespace App\Indicators;


/**
 * Class BurstSameMinute
 * If a number of reviews come in within the same time frame.
 * Knock 40% points off if 2 or more come through in the same minute, 20% points if they come through in the same hour.
 */
class BurstSameMinute extends AbstractIndicator
{
    protected $value = -40;
}
