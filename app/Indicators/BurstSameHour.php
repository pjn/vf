<?php

namespace App\Indicators;


/**
 * Class BurstSameHour
 * If a number of reviews come in within the same time frame.
 * Knock 40% points off if 2 or more come through in the same minute, 20% points if they come through in the same hour.
 */
class BurstSameHour extends AbstractIndicator
{
    protected $value = -20;
}
