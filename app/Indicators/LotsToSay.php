<?php

namespace App\Indicators;


/**
 * Class LotsToSay
 * Genuine reviewers tend to say less - knock 0.5% points off for each review that contains more than 100 words.
 */
class LotsToSay extends AbstractIndicator
{
    protected $value = -0.5;
}
