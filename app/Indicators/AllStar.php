<?php

namespace App\Indicators;


/**
 * Class AllStar
 * Non-genuine reviews are likely to have a five star rating - take 2% points off the integrity score
 * for each review that has 5 stars; quadruple the penalty if the average is under 3.5 stars.
 */
class AllStar extends AbstractIndicator
{
    protected $value = -2;
}
