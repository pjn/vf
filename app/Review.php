<?php

namespace App;


use DateTime;

class Review
{
    const RATING_MAX = 5;
    const RATING_MIN = 1;
    const RATING_LOW = 3.5;
    const WORDCOUNT_HIGH = 100;
    const WORDCOUNT_PATTERN = "/^([1-9][0-9]*)[ ]words?$/";

    private $datetime;
    private $advisor;
    private $solicited;
    private $tag;
    private $wordcount;
    private $rating;

    public function __construct(array $data=[])
    {
        $this->setDatetime($data["datetime"]);
        $this->setAdvisor($data["advisor"]);
        $this->setSolicited($data["solicited"]);
        $this->setTag($data["tag"]);
        $this->setWordcount($data["wordcount"]);
        $this->setRating($data["rating"]);
    }

    /**
     * @return string \ DateTime
     */
    public function getDatetime($format=null)
    {
        if (is_null($format))
        {
            return $this->datetime;
        }

        return $this->datetime->format($format);
    }

    /**
     * @param string $datetime
     */
    public function setDatetime($datetime)
    {
        $this->datetime = new DateTime($datetime);
    }

    /**
     * @return string
     */
    public function getAdvisor()
    {
        return $this->advisor;
    }

    /**
     * @param string $advisor
     */
    public function setAdvisor($advisor)
    {
        $this->advisor = $advisor;
    }

    /**
     * @return bool
     */
    public function isSolicited()
    {
        return $this->solicited;
    }

    /**
     * @param bool $solicited
     */
    public function setSolicited($solicited)
    {
        $this->solicited = ($solicited == "solicited");
    }

    /**
     * @return string
     */
    public function getTag()
    {
        return $this->tag;
    }

    /**
     * @param string $tag
     */
    public function setTag($tag)
    {
        $this->tag = (string) $tag;
    }

    /**
     * @return int
     */
    public function getWordcount()
    {
        return $this->wordcount;
    }

    /**
     * @param int $wordcount
     */
    public function setWordcount($wordcount)
    {
        preg_match(Review::WORDCOUNT_PATTERN, $wordcount, $matches);
        $this->wordcount = (int) $matches[1];
    }

    /**
     * @return float
     */
    public function getRating()
    {
        return $this->rating;
    }

    /**
     * @param float $rating
     */
    public function setRating($rating)
    {
        $this->rating = (float) strlen($rating);
    }
}
