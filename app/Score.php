<?php

namespace App;

use App\Events\Event;
use App\Events\ScoreAlert;
use App\Events\ScoreWarning;
use App\Indicators\Indicator;

class Score
{
    const SCORE_MAX = 100;
    const SCORE_WARNING = 70;
    const SCORE_ALERT = 50;

    private $score;
    private $indicators = [];
    private $advisor;

    public function __construct(Advisor $advisor)
    {
        $this->score = (float) Score::SCORE_MAX;
        $this->advisor = $advisor;
    }

    public function __toString()
    {
        return $this->getScore(true);
    }

    /**
     * @return float|string - formatted percent score
     */
    public function getScore($formatted=false)
    {
        if ($formatted)
        {
            return sprintf("%.1f", $this->score);
        }

        return $this->score;
    }

    /**
     * @return bool
     */
    public function hasIndicators()
    {
        return (bool) $this->indicators;
    }

    /**
     * @return array Indicator $indicators
     */
    public function getIndicators()
    {
        return $this->indicators;
    }

    /**
     * @param array Indicator $indicators
     */
    public function applyIndicators(array $indicators=[])
    {
        $score = $this->score;

        foreach ($indicators as $indicator)
        {
            $this->indicators[] = $indicator;
            $score += $indicator->apply();
        }

        if ($score > Score::SCORE_MAX)
        {
            $score = Score::SCORE_MAX;
        }

        $this->score = $score;

        if ($this->score <= Score::SCORE_WARNING)
        {
            if ($this->score <= Score::SCORE_ALERT)
            {
                $msg = sprintf("%s has been de-activated due to a low trusted review score", $this->advisor->getName());
                event(new ScoreAlert($msg));
            }
            else
            {
                $msg = sprintf("%s has a trusted review score of %s", $this->advisor->getName(), $this->score);
                event(new ScoreWarning($msg));
            }
        }
        else
        {
            $trail = array_map(function(Indicator $indicator) {
                return sprintf("%s => %.1f", get_class($indicator), $indicator->apply());
            }, $indicators);

            $message = sprintf("Jon has a trusted review score of: %.1f. Indicators applied: [%s] ", $this->score, implode(" | ", $trail));
            event(new Event($message));
        }
    }

    public function evaluateScore()
    {
        if ($this->score <= Score::SCORE_WARNING)
        {
            if ($this->score <= Score::SCORE_ALERT)
            {
                $msg = sprintf("%s has been de-activated due to a low trusted review score", $this->advisor->getName());
                event(new ScoreAlert($msg));
            }
            else
            {
                $msg = sprintf("%s has a trusted review score of %s", $this->advisor->getName(), $this->score);
                event(new ScoreWarning($msg));
            }
        }
    }
}

