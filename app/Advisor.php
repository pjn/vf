<?php

namespace App;


use App\Indicators\AllStar;
use App\Indicators\BurstSameHour;
use App\Indicators\BurstSameMinute;
use App\Indicators\LotsToSay;
use App\Indicators\SameDevice;
use App\Indicators\Solicited;
use DateInterval;

class Advisor
{
    private $name;
    private $score;
    private $reviews = [];

    /**
     * Advisor constructor.
     * @param string $name
     */
    public function __construct($name)
    {
        $this->setName($name);
        $this->score = new Score($this);
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = ucfirst($name);
    }

    /**
     * @return Score
     */
    public function getScore()
    {
        return $this->score;
    }

    /**
     * @return float - advisor's rating based on average review rating
     */
    public function getRating()
    {
        $rating = [];

        foreach ($this->reviews as $review)
        {
            $rating[] = $review->getRating();
        }

        return round(array_sum($rating) / count($rating), 1);
    }

    /**
     * @param int $rating
     */
    public function setRating($rating)
    {
        $this->rating = (float) $rating;
    }

    /**
     * @return bool
     */
    public function hasReviews()
    {
        return (bool) $this->reviews;
    }
    /**
     * @return array Review $reviews
     */
    public function getReviews()
    {
        return $this->reviews;
    }

    /**
     * @param array $reviews
     */
    public function setReviews(array $reviews=[])
    {
        foreach ($reviews as $review)
        {
            $this->addReview($review);
        }
    }

    /**
     * Adds review. Updates score by delegating aggregated indicators to Score class.
     * @param Review $review
     */
    public function addReview(Review $review)
    {
        $indicators = [];

        if ($review->isSolicited())
        {
            $indicators[] = new Solicited();
        }

        if ($review->getRating() == Review::RATING_MAX)
        {
            if ($this->getRating() < Review::RATING_LOW)
            {
                $indicators[] = new AllStar();
                $indicators[] = new AllStar();
                $indicators[] = new AllStar();
            }

            $indicators[] = new AllStar();
        }

        if ($review->getWordcount() > Review::WORDCOUNT_HIGH)
        {
            $indicators[] = new LotsToSay();
        }

        if ($this->hasReviews())
        {
            $reviewDatetime = $review->getDatetime();
            $reviewTimestamp = $reviewDatetime->getTimestamp();

            foreach ($this->reviews as $r)
            {
                $rDatetime = $r->getDatetime();
                $minuteClone = clone $rDatetime;
                $minute = $minuteClone->add(new DateInterval('PT59S'));
                $minuteTimestamp = $minute->getTimestamp();
                $hourClone = clone $rDatetime;
                $hour = $hourClone->add(new DateInterval('PT1H'));
                $hourTimestamp = $hour->getTimestamp();


                if ($reviewTimestamp <= $hourTimestamp)
                {
                    if ($reviewTimestamp <= $minuteTimestamp)
                    {
                        $indicators[] = new BurstSameMinute();
                    }
                    else
                    {
                        $indicators[] = new BurstSameHour();
                    }
                    break;
                }

                if ($r->getTag() == $review->getTag())
                {
                    $indicators[] = new SameDevice();
                }
            }
        }

        $this->score->applyIndicators($indicators);

        $this->reviews[] = $review;
    }
}
